/*
   Green Sun Project
   Tamara - smart home module

   Version : Prototype-001
   Created by : Dawid Brelak
   Date : 06.08.2018

   Simple REST controlled smart home module.
   It's designed to measure temperature and light level also allow control rgb led strip.
*/

// Libraries used to read temperature using Dallas DS18B20
#include <OneWire.h>
#include <DallasTemperature.h>

#include <SPI.h>
#include <Ethernet.h>
#include <aREST.h>
#include <avr/wdt.h>
/*
   Pin definition
   Analog 0 - Photoresistor
   Digital 2 -Dallas DS18B20 wire bus
   Digital 3 - Strip blue light
   Digital 5 - Strip green light
   Digital 6 - Strip red light
*/
const int PHOTORESISTOR = 0;
const int DALLAS_WIRE = 2;

const int BLUE_LIGHT = 3;
const int GREEN_LIGHT = 5;
const int RED_LIGHT = 6;

// Variables
int light = 0;
double temperature = 0.0;
String strip="000000";
int red = 0;
int blue = 0;
int green = 0;

// Setup a oneWire instance to communicate with ANY OneWire devices
OneWire oneWire(DALLAS_WIRE);
// Set oneWire reference to Dallas Temperature sensor.
DallasTemperature sensors(&oneWire);

// Enter a MAC address for your controller below.
byte mac[] = { 0x00, 0xA2, 0xDA, 0x0E, 0xFE, 0x40 };
// IP address in case DHCP fails
IPAddress ip(192, 168, 200, 51);

EthernetServer server(80);
aREST rest = aREST();

// Strip control functions declaration
int redControl(String color);
int greenControl(String color);
int blueControl(String color);
int stripControl(String rgb);
void setup(void)
{
  Serial.begin(115200);

  // hardware setup
  sensors.begin();
  pinMode(GREEN_LIGHT, OUTPUT);
  pinMode(RED_LIGHT, OUTPUT);
  pinMode(BLUE_LIGHT, OUTPUT);
  analogWrite(RED_LIGHT, red);
  analogWrite(BLUE_LIGHT, blue);
  analogWrite(GREEN_LIGHT, green);

  // rest read varialbles
  rest.variable("light", &light);
  rest.variable("temperature", &temperature);
  rest.variable("red", &red);
  rest.variable("green", &green);
  rest.variable("blue", &blue);
  rest.variable("strip", &strip);

  // rest function can be set by call function name ?param= value
  rest.function("redWrite", redControl);
  rest.function("greenWrite", greenControl);
  rest.function("blueWrite", blueControl);
  rest.function("stripWrite", stripControl);

  rest.set_id("11");
  rest.set_name("Green Sun Tamara");

  // Start the Ethernet connection and the server
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");
    Ethernet.begin(mac, ip);
  }
  server.begin();
  Serial.println("Green Sun Tamara online");
  Serial.println(Ethernet.localIP());

  // Start watchdog
  wdt_enable(WDTO_4S);
}
void loop(void)
{
  sensors.requestTemperatures();
  //measure temperature by first sensor
  temperature = sensors.getTempCByIndex(0);
  // read analog value from range 0 to 1024
  light = analogRead(PHOTORESISTOR);

  // Handle REST calls and listen for incoming clients
  EthernetClient client = server.available();
  rest.handle(client);
  wdt_reset();
}
String rgbToString(int r,int g,int b){
  String s="";
  if(r<16)
    s+="0";
  s+=String(r,HEX);
  if(g<16)
    s+="0";
  s+=String(g,HEX);
  if(b<16)
    s+="0";
  s+=String(b,HEX);
  return s;
}
// Strip control functions
int redControl(String color) {
   if (color.length() <= 0) {
    return 1;
  }
  int i =color.toInt();
  if(i>=0&&i<=255){
    red = i;
    analogWrite(RED_LIGHT, red);
    strip=rgbToString(red,green,blue);
    return 0;
  }
  return 1;
}
int greenControl(String color) {
   if (color.length() <= 0) {
    return 1;
  }
  int i =color.toInt();
  if(i>=0&&i<=255){
    green = i;
    analogWrite(GREEN_LIGHT, green);
    strip=rgbToString(red,green,blue);
    return 0;
  }
  return 1;
}
int blueControl(String color) {
  if (color.length() <= 0) {
    return 1;
  }
  int i =color.toInt();
  if(i>=0&&i<=255){
    blue = i;
    analogWrite(BLUE_LIGHT, blue);
    strip=rgbToString(red,green,blue);
    return 0;
  }
  return 1;
}
int stripControl(String rgb) {
  if (rgb.length() == 6) {
    //catch first incorrect param char
    char *check;
    //string to long
   long value = strtol (&rgb[0] , &check, 16);
    //change color if catched char is string end
    if (*check == '\0') {
      //get colors from long value RRGGBB
      strip=rgb;
      red = value >> 16;
      green = (value & 0x00ff00) >> 8;
      blue = (value & 0x0000ff);
      analogWrite(BLUE_LIGHT, blue);
      analogWrite(GREEN_LIGHT, green);
      analogWrite(RED_LIGHT, red);
      return 0;
    }
  }
  return 1;
}
