Green Sun Project Tamara - smart home module

 Version : Prototype-001
 Created by : Dawid Brelak 
 Date : 06.08.2018
 
# This module a simple REST controlled smart home module. 
It's designed to measure temperature and light level also allow control rgb led strip.

# Available components :
http://<ip address>/temperature - read current temperature
http://<ip address>/light - read current light level

http://<ip address>/strip?param=<hex color> - set strip color
	* single strip channel allow set value from 0 to 255
	
http://<ip address>/r?param=<value> - control strip red color
http://<ip address>/g?param=<value> - control strip green color
http://<ip address>/b?param=<value> - control strip blue color

http://<ip address>/r - read red color value 
http://<ip address>/g - read green color value 
http://<ip address>/b - read blue color value 

# Used components :
* Adruino Uno
* Adruino Ethernet
* Phototransistor (A0)
* Dallas DS18B20 (D2)
* 3x n-mosfet IRFZ44N (D3,D5,D6) + 10 cm rgb led strip 
* L7805CV (this is optionally can be replaced by 5V DC and 12V power supplies 
			or step-down converter that is connected to 12v DC )