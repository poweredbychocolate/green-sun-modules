/*
   Green Sun Project
   Eyes - smart home module

   Version : Prototype-001
   Created by : Dawid Brelak
   Date : 03.09.2018

   Simple REST controlled smart home module.
   Have two relays ans two buttons that state can be read.
*/

// Rest and esp library
#include <ESP8266WiFi.h>
#include <aREST.h>

// WiFi parameters
const char* ssid = "ssid";
const char* password = "password";


/*
   Pin definition
   D0 (GPOI16) -First button
   D1 (GPOI05) - Second button
   D5 (GPOI14) -First relay
   D6 (GPOI12) - Second relay
*/
const int SWITCH1 = 5;
const int SWITCH2 = 16;
const int RELAY1 =  12;
const int RELAY2 =  14;

// interval for buttons
const unsigned long INTERVAL = 500;

// Hardware state variables
bool relay1State = LOW;
bool relay2State = LOW;
bool switch1State = LOW;
bool switch2State = LOW;
bool controlRelays = true;

// millis for button
unsigned long currentMillis = 0;
unsigned long switch1Millis = 0;
unsigned long switch2Millis = 0;

// Create aREST instance
aREST rest = aREST();
// The port to listen for incoming TCP connections
const int LISTEN_PORT = 80;
// Create an instance of the server
WiFiServer server(LISTEN_PORT);

// Rest functions declaration
int relay1Control(String value);
int relay2Control(String value);
int relaysControl(String value);

void setup(void)
{
  // Give name & ID to the device
  rest.set_id("12");
  rest.set_name("Green Sun Eyes");

  Serial.begin(115200);

  // hardware setup
  pinMode(SWITCH1, INPUT);
  pinMode(SWITCH2, INPUT);
  pinMode(RELAY1, OUTPUT);
  pinMode(RELAY2, OUTPUT);
  digitalWrite(RELAY1, !relay1State);
  digitalWrite(RELAY2, !relay2State);

  // rest read varialbles
  rest.variable("switch1", &switch1State);
  rest.variable("switch2", &switch2State);
  rest.variable("relay1", &relay1State);
  rest.variable("relay2", &relay2State);
  rest.variable("controlRelays", &controlRelays);
  // Control function
  rest.function("controlRelaysWrite", relaysControl);
  rest.function("relay1Write", relay1Control);
  rest.function("relay2Write", relay2Control);

  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Green Sun Eyes connected");

  // Start the server
  server.begin();
  Serial.println("Green Sun Eyes address");

  // Print the IP address
  Serial.println(WiFi.localIP());
//  Serial.println(true);
 // Serial.println(HIGH);
 // Serial.println(false);
 // Serial.println(LOW);
}

void loop() {
  //Buttons state update
  currentMillis = millis();
  if (currentMillis - switch1Millis >= INTERVAL && digitalRead(SWITCH1)) {
    Serial.println("Switch 1");
    switch1State = !switch1State;
        if(controlRelays==true&&switch1State==HIGH){
         digitalWrite(RELAY1, LOW);
         relay1State = !LOW;
    }
        if(controlRelays==true&&switch1State==LOW){
         digitalWrite(RELAY1,HIGH );
         relay1State = !HIGH;
    }
    switch1Millis = currentMillis;
  }
  if (currentMillis - switch2Millis >= INTERVAL && digitalRead(SWITCH2)) {
    Serial.println("Switch 2");
    switch2State = !switch2State;
    if(controlRelays==true&&switch2State==HIGH){
         digitalWrite(RELAY2, LOW);
         relay2State = !LOW;
    }
        if(controlRelays==true&&switch2State==LOW){
         digitalWrite(RELAY2,HIGH );
         relay2State = !HIGH;
    }
    switch2Millis = currentMillis;
  }

  // Handle REST calls
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  while (!client.available()) {
    delay(10);
  }
  rest.handle(client);

}

// Custom rest function
//Relays
int relay1Control(String value) {
  Serial.println("Relay 1 Control");
  if (value=="1") {
    digitalWrite(RELAY1, LOW);
    relay1State = !LOW;
    if(controlRelays==true){
      switch1State = HIGH;
    }
    return 0;
  }
  if (value=="0") {
    digitalWrite(RELAY1, HIGH);
    relay1State = !HIGH;
    if(controlRelays==true){
      switch1State = LOW;
    }
    return 0;
  }
  return 1;
}

int relay2Control(String value) {
Serial.println("Relay 2 Control");
    if (value=="1") {
    digitalWrite(RELAY2, LOW);
    relay2State = !LOW;    
    if(controlRelays==true){
      switch2State = HIGH;
    }
    return 0;
  }
  if (value=="0") {
    digitalWrite(RELAY2, HIGH);
    relay2State = !HIGH;
      if(controlRelays==true){
      switch2State = LOW;
    }
    return 0;
  }
  return 1;
}
int relaysControl(String value) {
Serial.println("Relays Control Mode");
    if (value=="0") {
     controlRelays=false;
    return 0;
  }
  if (value=="1") {
    controlRelays=true;
    return 0;
  }
  return 1;
}
