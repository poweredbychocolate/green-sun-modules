Green Sun Project Eyes - smart home module

 Version : Prototype-001
 Created by : Dawid Brelak 
 Date : 03.09.2018
 
# Simple REST controlled smart home module.
	Have two relays ans two buttons that state can be read.
	
# Network configuration.
   * ssid - wireless network name
   * password - wireless network password
   
# Available components :
http://<ip address>/switch1 - first switch state (0 if switched off and 1 if switched on)
http://<ip address>/switch2 - second switch state (0 if switched off and 1 if switched on)
http://<ip address>/relay1 - first relay state (0 if open and 1 if closed)
http://<ip address>/relay2 - second relay state (0 if open and 1 if closed)

* value - on(switch oon a switches close relay) / off(switch off a switches or open relay)
	
http://<ip address>/switch1?param=<value>
http://<ip address>/switch2?param=<value>
http://<ip address>/relay1?param=<value>
http://<ip address>/relay2param=<value>

# Used components :
* Nodemcu v3 
* HL-52S Relay Module
* Two buttons