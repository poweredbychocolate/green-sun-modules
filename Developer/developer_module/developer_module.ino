/*
   Green Sun Project
   Developer - smart home module for connection test

   Version : Prototype-001
   Created by : Dawid Brelak
   Date : 27.11.2018
   Simple REST controlled smart home module 
   that allow others system test conection iniplementation.
*/

// Rest and esp library
#include <ESP8266WiFi.h>
#include <aREST.h>

// WiFi parameters
const char* ssid = "ssid";
const char* password = "password";

//Rest Resources
bool booleanResource=random(0,1);
String characterResource="Read Character Resource";
int integerResource=random(0,255);
float floatResource=random(0,25500)/ 100.0;

// Create aREST instance
aREST rest = aREST();
// The port to listen for incoming TCP connections
const int LISTEN_PORT = 80;
// Create an instance of the server
WiFiServer server(LISTEN_PORT);

// Rest functions declaration
int writeBoolean(String command);
int writeCharacter(String command);
int writeInteger(String command);
int writeFloat(String command);

void setup(void)
{

  // Give name & ID to the device
  rest.set_id("10");
  rest.set_name("Green Sun Developer");

  Serial.begin(115200);

  // rest read varialbles
  rest.variable("boolean", &booleanResource);
  rest.variable("character", &characterResource);
  rest.variable("integer", &integerResource);
  rest.variable("float", &floatResource);
  // Control function
  rest.function("booleanWrite", writeBoolean);
  rest.function("characterWrite", writeCharacter);
  rest.function("integerWrite", writeInteger);
  rest.function("floatWrite", writeFloat);

  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Green Sun Developer connected");

  // Start the server
  server.begin();
  Serial.println("Green Sun Developer address");

  // Print the IP address
  Serial.println(WiFi.localIP());
  // Serial.println(false);
  // Serial.println(true);
}

void loop() {
  // Handle REST calls
  WiFiClient client = server.available();
  if (!client) {
    return;
  }
  while (!client.available()) {
    delay(10);
  }
  rest.handle(client);

}
int writeBoolean(String command){
   Serial.println("Write Boolean Control");
   if (command=="1") {
    booleanResource=true;
  }else if(command=="0"){
    booleanResource=false;
  }else{
    return 1;
  }
  return 0;
}
int writeCharacter(String command){
  Serial.println("Write Character Control");
  if(command.length()<=0){
    characterResource="";
    return 1;
  }
  characterResource=command;
  return 0;
}
int writeInteger(String command){
   Serial.println("Write Integer Control");
  if(command.length()<=0){
    return 1;
  }  
   int i =command.toInt();  
   if(i>=0&&i<=255){
     integerResource= i;
     return 0;
   }
  return 1;
}
int writeFloat(String command){
   Serial.println("Write Float Control");
  if(command.length()<=0){
    return 1;
    }
    float f= command.toFloat();
     if(f>=0.0&&f<=255.0){
      floatResource=f;
     return 0;
   }
   return 1;
}
