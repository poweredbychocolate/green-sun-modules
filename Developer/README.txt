Green Sun Project Developer - smart home module

 Version : Prototype-001
 Created by : Dawid Brelak 
 Date : 27.11.2018
 
# This module a simple REST controlled smart home module. 
It's developer module for testing rest resource. This module is communication template.

# Available resources :
http://<ip address>/boolean - read boolean resource
http://<ip address>/character - read character resource
http://<ip address>/integer - read integer resource
http://<ip address>/float - read float resource

http://<ip address>/booleanWrite?param=<value> - set boolean resource
http://<ip address>/characterWrite?param=<value> - write character resource
http://<ip address>/integerWrite?param=<value> - write integer resource
http://<ip address>/floatWrite?param=<value> - write float resource


